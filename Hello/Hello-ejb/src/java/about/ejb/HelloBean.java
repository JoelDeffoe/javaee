/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package about.ejb;

import javax.ejb.Stateless;


@Stateless
public class HelloBean implements HelloBeanRemote {

    @Override
    public String sayHello(){
         System.out.println("FYI: sayHello was called ");
         return "Hello from Strange Beans, the Giant is upon us.";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todo.ejb;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import static javax.ejb.LockType.READ;
import static javax.ejb.LockType.WRITE;
import javax.ejb.Singleton;
import todo.entities.TodoItem;

/**
 *
 * @author ipd11
 */
@Singleton
public class Quiz1TodoDataEJB implements Quiz1TodoDataEJBRemote {
    
    private ArrayList<TodoItem> todoList;
    
     @PostConstruct
     void initialize() {
        todoList = new ArrayList<>();
    }
     
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Lock(READ)
    @Override
    public TodoItem[] getAllTodoItems() {
        System.out.println("NNN: getAllShouts was called"+ todoList);
        return todoList.toArray(new TodoItem[0]); //To change body of generated methods, choose Tools | Templates.
    }

    @Lock(WRITE)
    @Override
    public void addTodoItem(TodoItem item) {
        System.out.println("NNN: addShout called with: " + todoList);
        todoList.add();
        //To change body of generated methods, choose Tools | Templates.
    }
}

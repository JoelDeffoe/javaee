/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package counttime.ejb;

import javax.ejb.Lock;
import static javax.ejb.LockType.READ;
import static javax.ejb.LockType.WRITE;
import javax.ejb.Singleton;

/**
 *
 * @author ipd11
 */
@Singleton
public class CounterBean implements CounterBeanRemote {

    @Lock(WRITE)
    @Override
    public void registerVisit() {
        counter++;
    } // increments counter of visits
    private int counter;

    @Lock(READ)
    @Override
    public int getTotalVisitCount() {

        return counter;
    }

    // returns current value of counter
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
